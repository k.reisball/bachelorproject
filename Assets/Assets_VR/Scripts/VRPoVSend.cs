﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using TMPro;

public class VRPoVSend : MonoBehaviourPun, IPunObservable
{
    public bool VRstartSending = false;
    ARSendPoV arBool = null;
    GameObject ARPoVScreen = null;
    bool onlyOneCoroutineA = false;
    bool onlyOneCoroutineB = false;
    bool onlyOneCoroutineC = false;
    bool onlyOneCoroutineD = false;
    TextMeshProUGUI requestedaidAR = null;
    GameObject requestaidVR = null;
    GameObject requestSent = null;
    GameObject VRPoVScreen = null;
    GameObject VRPoVCamera = null;

    public void newSceneLoadFunction()
    {
        if(!photonView.IsMine)  // Get a reference to AR-SendPoVBool, AR-PoV Screen
        {
            arBool = GameObject.Find("ARPoVSender").GetComponent<ARSendPoV>();
            ARPoVScreen = GameObject.Find("Head").transform.GetChild(0).gameObject;
            requestedaidAR = GameObject.Find("ARRequest").transform.GetChild(0).GetChild(0).GetComponent<TextMeshProUGUI>();
        }
        else
        {
            requestSent = GameObject.Find("PoVRequested").transform.GetChild(1).gameObject;
            VRPoVScreen = GameObject.Find("ARPoVDisplay").transform.GetChild(0).gameObject;
            VRPoVCamera = GameObject.Find("VRLight").transform.GetChild(0).gameObject;
            requestaidVR = GameObject.Find("PoVRequested").transform.GetChild(0).gameObject;
        }

        // Reset coroutine bools, in case level got loaded mid-coroutine
        onlyOneCoroutineA = false;
        onlyOneCoroutineB = false;
        onlyOneCoroutineC = false;
        VRstartSending = false;
    }

    // Try to find references every frame if they are null (Bad practise, is for testing)
    private void Update()
    {
        if(!photonView.IsMine)
        {
            if (arBool == null || ARPoVScreen == null || requestedaidAR == null)
                newSceneLoadFunction();
        }
        else
        {
            if (requestSent == null || VRPoVCamera == null || VRPoVCamera == null || requestaidVR == null)
                newSceneLoadFunction();
        }
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if(stream.IsWriting)    // Network owner writes values here
        {
            stream.SendNext(VRstartSending);

            if(onlyOneCoroutineA == false && VRstartSending == true)
            {
                StartCoroutine(DelayedFalse());
            }
        }
        else    // Network Receiver
        {
            VRstartSending = (bool)stream.ReceiveNext();

            if(VRstartSending == true)
            {
                if(arBool.ARstartSending == true)
                {
                    Debug.Log("Calling RPC AR");
                    photonView.RPC("BothConfirmed", RpcTarget.All);
                }
                else
                {
                    StartCoroutine(Requested());
                }
            }
        }
    }

       // Method that gets called on all clients
    [PunRPC]
    void BothConfirmed(PhotonMessageInfo info)
    {
        
        Debug.Log("RPC called VR");
        if(photonView.IsMine == true)
        {
            if(onlyOneCoroutineB == false)
            {
                StartCoroutine(StartPoVexchangeVR());
            }
        }
        else
        {
            if(onlyOneCoroutineD == false)
            {
                StartCoroutine(StartPoVexchangeAR());
            }
        }
    }

    IEnumerator DelayedFalse()
    {
        onlyOneCoroutineA = true;

        if(requestSent != null)
        {
            requestSent.SetActive(true);
        }

        // Give the other player 10 seconds to respond
        yield return new WaitForSeconds(10f);

        if(requestSent != null)
        {
            requestSent.SetActive(false);
        }

        VRstartSending = false;

        onlyOneCoroutineA = false;
    }

    IEnumerator StartPoVexchangeAR()
    {
        onlyOneCoroutineD = true;

        if(ARPoVScreen != null)
        {
            if(requestedaidAR != null)
            {
                requestedaidAR.text = "Request perspective exchange";
                requestedaidAR.color = Color.white;
            }

            ARPoVScreen.SetActive(true);

            yield return new WaitForSeconds(15f);

            ARPoVScreen.SetActive(false);
        }

        onlyOneCoroutineD = false;
    }

        IEnumerator StartPoVexchangeVR()
    {
        onlyOneCoroutineB = true;

        if(VRPoVScreen != null && VRPoVCamera != null) 
        {
            VRPoVCamera.SetActive(true);
            VRPoVScreen.SetActive(true);

            if(requestaidVR != null)
            {
                requestaidVR.SetActive(false);
            }

            if(requestSent != null)
            {
                requestSent.SetActive(false);
            }

            yield return new WaitForSeconds(15f);

            VRPoVCamera.SetActive(false);
            VRPoVScreen.SetActive(false);
        }

        onlyOneCoroutineB = false;
    }

    IEnumerator Requested()
    {
        onlyOneCoroutineC = true;

        if(requestedaidAR != null)
        {
            requestedaidAR.text = "Prespective exchange requested...\nConfirm by pressing here.";
            requestedaidAR.color = Color.red;

            // Activate only 7.5 sec to preft confusion due to lag
            yield return new WaitForSeconds(7.5f);

            requestedaidAR.text = "Request perspective exchange";
            requestedaidAR.color = Color.white;
        }

        onlyOneCoroutineC = false;
    }
}
