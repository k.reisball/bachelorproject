﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class VRRequestPoV : MonoBehaviour
{
    VRPoVSend vrpovsender = null;
    SteamVR_Behaviour_Pose pose = null;
    public SteamVR_Action_Boolean touchpadpressed;


    private void Awake() 
    {
        pose = GetComponent<SteamVR_Behaviour_Pose>(); // Get Controller Reference
    }

    // Start is called before the first frame update
    void Start()
    {
        // Get reference to VR-PoV-Script
        vrpovsender = GameObject.Find("VRPoVSender").GetComponent<VRPoVSend>();
    }

    private void Update() 
    {
        if(touchpadpressed.GetStateUp(pose.inputSource))
        {
            Debug.Log("Left touchpad pressed");
            if(vrpovsender == null)
            {
                vrpovsender = GameObject.Find("VRPoVSender").GetComponent<VRPoVSend>();
                if (vrpovsender == null)
                    return;
            }

            // Could pack this in a Coroutine and add a cooldown once pressed here to prevent spamming it
            vrpovsender.VRstartSending = true;
        }
    }

}
