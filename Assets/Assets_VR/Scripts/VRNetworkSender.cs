﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Photon.Pun;

public class VRNetworkSender : MonoBehaviourPun
{
    
    GameObject VRcamera = null;
    GameObject VRPlayer = null;
    [SerializeField] SendPushedBoulder sendBoulderscript;
    [SerializeField] VRSendHighlightedObject sendVRHighlightscript;
    [SerializeField] SendReturnToAR sendReturnscript;
    [SerializeField] VRPoVSend vrPOVsendscript;
    [SerializeField] VRSendLaserPointer vrLaserpointerscript;

    private void Start() 
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        // Call newSceneLoadfunction of this object and all children objects (except for lobby-ready-checker)
        newSceneLoadFunction();
        sendBoulderscript.newSceneLoadFunction();
        sendVRHighlightscript.newSceneLoadFunction();
        sendReturnscript.newSceneLoadFunction();
        vrPOVsendscript.newSceneLoadFunction();
        vrLaserpointerscript.newSceneLoadFunction();
    }

    void newSceneLoadFunction()
    {
        // Find objects that are supposed to be affected by this network component
        // And reparent this object to the Level so local transforms can be used
        if(photonView.IsMine)
        {
            ToolsUsedCounter.clearCounters();
            VRcamera = GameObject.Find("Camera (eye)");
            transform.SetParent(GameObject.Find("LevelVR").transform);
        } else
        {
            // The following makes so it currently only works, when the AR player connects and spawns his ARobject first
            // Should be fixed by adding a lobby-screen later
            VRPlayer = GameObject.Find("VRPlayer");
            transform.SetParent(GameObject.Find("LevelAR").transform);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(photonView.IsMine)
        {
            // I am the VR Scene
            if(VRcamera == null)
            {
                newSceneLoadFunction();
                if (VRcamera == null)
                    return;
            }

            transform.position = VRcamera.transform.position;
            transform.rotation = VRcamera.transform.rotation;
        } else
        {
            // I am the AR Scene
            // Set Avatar position
            if(VRPlayer == null)
            {
                newSceneLoadFunction();
                if (VRPlayer == null)
                    return;
            }

            VRPlayer.transform.localPosition = transform.localPosition;

            // Set Avater up-axis rotation
            Vector3 temp = VRPlayer.transform.localEulerAngles;
            temp.y = transform.localEulerAngles.y;
            VRPlayer.transform.localEulerAngles = temp;

            // Set Avatar head position
            temp = transform.localEulerAngles;
            temp.y = 0;
            VRPlayer.transform.GetChild(0).localEulerAngles = temp;
        }
    }
}
