﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class RotateAroundTest : MonoBehaviour
{
    [SerializeField] GameObject target;

    // When using the return function, I want the player to face the same direction as the last time they were here (before they used their action)
    // For that I need to rotate the players Y-Axis, unfortunately rotateing the camera directly does not work, as its rotation get hardwritten to by the real-world HMD rotation
    // Rotating the whole CameraRig does not work either, because it will rotate the whole player space orientation (virtual steamvr grid)
    // So we need a parent object on the camera to rotate it, but since the Cameras position is moving, while the parents is not, the pivot point is wrong if you just rotate the parent
    // Instead I rotate the Parent around the camera, which essentially results in the camera being rotate around itself. 

    // Using a parentobject to the VR camera also requires you to replace all SteamVR_Render.Top().origin with an actual reference to the [Camera Rig] object as the origin will just check for the camera parent
    public void rotateVRcameraYaxis(float rotationvalue)
    {
        transform.RotateAround(target.transform.position, Vector3.up, rotationvalue);
    }
}
