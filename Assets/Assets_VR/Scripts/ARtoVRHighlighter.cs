﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ARtoVRHighlighter : MonoBehaviour
{
    [SerializeField] ControllerRaycast vrHighlighter;
    float lerptime = 0.8f;
    [HideInInspector] public bool isHighlighted = false;
    [HideInInspector] public Coroutine lastCoroutine = null; // Needed to get an Identifier for the current active Highlight so we can stop it if needed
    GameObject lastHighlight = null;
    [SerializeField] GameObject playercamera;
    [SerializeField] GameObject directionArrow;

    
    public void ARtoVRHighlight(string objectToHighlight)
    {
        Debug.Log("Highlighter being called with: " + objectToHighlight);
        GameObject temp = GameObject.Find(objectToHighlight);   // Find corresponding object in my Scene
        // If scenes are fused add a check for AR or VR visible Layer here
        if(temp != null)
        {
            if(!isHighlighted)  // if nothing is being highlighted, highlight clicked object
            {
                if(temp.GetComponent<Outline>() != null)
                {
                    if(temp.GetComponent<Outline>().enabled == true)
                    {   
                        // Is highlighted by VR player, stop his highlighting
                        vrHighlighter.StopCoroutine(vrHighlighter.lastCoroutine);
                        vrHighlighter.isHighlighted = false;
                    }

                    // Start AR Highlighting
                    lastCoroutine = StartCoroutine(ARHighlighted(temp));
                    lastHighlight = temp;
                }
            }
            else    // Something is being highlighted
            {
                if(lastHighlight == temp)   // Forbids rehighlighting the same object while it is being highlighted, but allows it again when highlight is done
                {
                    return;
                }

                StopCoroutine(lastCoroutine);   // Stop Last Highlight
                directionArrow.SetActive(false);

                if(lastHighlight.GetComponent<Outline>() != null)   // And turn last objects Outline off
                {
                    lastHighlight.GetComponent<Outline>().enabled = false;
                    lastHighlight.GetComponent<Outline>().OutlineWidth = 0f;
                }

                isHighlighted = false;       

                lastCoroutine = StartCoroutine(ARHighlighted(temp));   // Restart Highlighting with new Object
                lastHighlight = temp;
            }
        }
    }

    IEnumerator ARHighlighted(GameObject highlightThis)
    {
        // I am not checking if script is already highlighting an object, to give some leniency for network lag spikes
        // Also it should only be send, when the highlight function on the AR end goes through, so the it is limited by that timer

        // Also not checking if the Object is already highlight, so overwriting each others highlights is possible and wanted behaviour

        if(highlightThis.transform.GetComponent<Outline>() != null)
        {
            isHighlighted = true;
            ToolsUsedCounter.ar_highlight++;

            // Get Outlinescript of hovered object and activate
            Outline outlinescript = highlightThis.transform.GetComponent<Outline>();
            outlinescript.enabled = true;
            outlinescript.OutlineColor = Color.red;
            outlinescript.OutlineWidth = 0f;

            for(int i = 0; i < 4; i++)
            {
                float currentLerpTime = 0f;
                float percent = 0f;

                while(percent < 1)  // Same lerping as for pushing object
                {
                    currentLerpTime += Time.deltaTime;
                    if(currentLerpTime >= lerptime)
                    {
                        currentLerpTime = lerptime;
                    }
                    percent = currentLerpTime/lerptime;
                    outlinescript.OutlineWidth = Mathf.Lerp(0f, 10f, percent);
                    yield return null;

                    // Check every frame if object is in player FoV
                    isObjectInFov(highlightThis);
                }

                currentLerpTime = 0f;
                percent = 0f;

                while(percent < 1)  
                {
                    currentLerpTime += Time.deltaTime;
                    if(currentLerpTime >= lerptime)
                    {
                        currentLerpTime = lerptime;
                    }
                    percent = currentLerpTime/lerptime;
                    outlinescript.OutlineWidth = Mathf.Lerp(10f, 0f, percent);
                    yield return null;

                    // Check every frame if object is in player FoV
                    isObjectInFov(highlightThis);
                }
            }

            // Deactivate arrow direction help, in case it was on
            directionArrow.SetActive(false);

            outlinescript.enabled = false;
            isHighlighted = false;
        }
    }

    // In case the AR highlighted object is outside the VR's player FoV we will provide a visual aid
    bool isObjectInFov(GameObject objectHighlighted)
    {   
        // First Calculate the objects middlepoints, since it's orientation point does not work for my walls, their pivot is in the center of the floor they are attached to
        Vector3 objectposition;
        if(objectHighlighted.GetComponent<MeshRenderer>() != null)
        {
            objectposition = objectHighlighted.GetComponent<MeshRenderer>().bounds.center;
        }
        else
        {
            objectposition = objectHighlighted.transform.position;
        }

        // Calculate the direction vector between playercamera and object
        Vector3 direction3D = objectposition - playercamera.transform.position;

        // Normalize the two direction vectors
        Vector3 normalizedCam = playercamera.transform.forward.normalized;
        Vector3 normalizedObject = direction3D.normalized;

        // Calculate tha angle between them
        float AdotB = Vector3.Dot(normalizedCam, normalizedObject);
        float angle = Mathf.Acos(AdotB) * Mathf.Rad2Deg;

        if(angle > 35.5f)  // Min angle to be outside of FoV, might need some finetuning
        { 
            // Make the arrow look at the target
            directionArrow.transform.LookAt(objectposition);
            directionArrow.SetActive(true);
            return true; // Outside of Fov
        }
        else
        {
            directionArrow.SetActive(false);
            return false; // Inside FoV
        }
    }
}

