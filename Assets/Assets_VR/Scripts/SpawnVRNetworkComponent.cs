﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class SpawnVRNetworkComponent : MonoBehaviourPun
{
    [SerializeField] GameObject VRLobbyReadySender;
    [SerializeField] GameObject VRNetworkComponent;

  private void Awake() 
    {
      // Instantiate the VR Network-Player Proxy
      GameObject VRready = PhotonNetwork.Instantiate(VRLobbyReadySender.name, Vector3.zero, Quaternion.identity);
      GameObject VRproxy = PhotonNetwork.Instantiate(VRNetworkComponent.name, Vector3.zero, Quaternion.identity);
    }
}
