﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class VRReadySend : MonoBehaviourPun, IPunObservable
{
    ARReadyCheck arReadyScript;
    VRReadyCheck vrReadyScript;
    bool otherPlayerReady = false;
    bool oneCoroutine = false;

    private void Awake() 
    {
        if(photonView.IsMine == true)
        {
            vrReadyScript = GameObject.Find("Intro").GetComponent<VRReadyCheck>();
        }
        else
        {
            arReadyScript =  GameObject.Find("Intro").GetComponent<ARReadyCheck>();
        }    
    }

    private void Update() 
    {
        if(otherPlayerReady && arReadyScript.arReady)
        {
            if(oneCoroutine == false)
            {
                StartCoroutine(loadScenewithDelay());
            }
        }    
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if(stream.IsWriting)    // Network owner writes values here
        {
            stream.SendNext(vrReadyScript.vrReady);
        }
        else    // Network Receiver
        {
            otherPlayerReady = (bool)stream.ReceiveNext();
        }
    }

    IEnumerator loadScenewithDelay()
    {
        Debug.Log("loadscene triggered");
        oneCoroutine = true;
        
        // Find parentobject
        GameObject parent = GameObject.Find("ARNetworkDependencies");
        Debug.Log("parent object: " + parent.name);
        // Reparent ARnetwork component
        GameObject.Find("ARNetworkSender(Clone)").transform.SetParent(parent.transform);

        // Reparent VRnetwork component
        GameObject.Find("VRNetworkSender(Clone)").transform.SetParent(parent.transform);

        // Get VRPlayer and parent it to ARDependencies
        GameObject.Find("VRPlayer").transform.SetParent(parent.transform);

        // Destroy old level
        GameObject temp = GameObject.Find("ARAnchor");
        if(temp.transform.childCount > 0)
        {
            Destroy(temp.transform.GetChild(0).gameObject);
        }

        //Check if PoV-Exchange is still active (no need to do on VR since we are not bringing the objects with us)
        if (GameObject.Find("VRPoVCam") != null)
        {
            GameObject.Find("VRPoVCam").SetActive(false);
        }

        yield return new WaitForSeconds(1f);

        PhotonNetwork.LoadLevel(3); // Loads Unity Scene level according to Build Index - AR Level 1

        oneCoroutine = false;
        Debug.Log("new scene loaded");
    }
}
