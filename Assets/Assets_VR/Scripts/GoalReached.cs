﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class GoalReached : MonoBehaviourPun
{
    [SerializeField] GameObject network_levelcomplete;

    private void OnTriggerEnter(Collider other) {
        if(other.name == "BodyCollider")
        {
            PhotonNetwork.Instantiate(network_levelcomplete.name, Vector3.zero, Quaternion.identity);
        }
        else
        {
            return;
        }
    }
}
