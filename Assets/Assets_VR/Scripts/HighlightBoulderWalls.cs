﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HighlightBoulderWalls : MonoBehaviour
{
    private void OnTriggerEnter(Collider other) 
    {
        if(other.name == "VR_Pointer")
        {

            if(gameObject.GetComponent<MeshRenderer>() != null)
            {
                // Check if pushing in the direction is possible
                Vector3 translationdirection;
                GameObject oppositeSide;
                switch(transform.name)
                {
                    case "Front":
                        translationdirection = Vector3.back;
                        oppositeSide = transform.parent.GetChild(3).gameObject;
                        break;
                    case "Back":
                        translationdirection = Vector3.forward;
                        oppositeSide = transform.parent.GetChild(2).gameObject;
                        break;
                    case "Left":
                        translationdirection = Vector3.right;
                        oppositeSide = transform.parent.GetChild(4).gameObject;
                        break;
                    case "Right":
                        translationdirection = Vector3.left;
                        oppositeSide = transform.parent.GetChild(5).gameObject;
                        break;
                    default:
                        oppositeSide = null;
                        return;
                }

                bool blocked = BoulderFree(translationdirection, oppositeSide);

                if(blocked)
                {
                    gameObject.GetComponent<MeshRenderer>().material.color = new Color32(255, 0, 0, 100);
                    gameObject.GetComponent<MeshRenderer>().material.SetColor("_EmissionColor", Color.red * 0.25f);
                }
                else
                {
                    gameObject.GetComponent<MeshRenderer>().material.color = new Color32(0, 137, 15, 100);
                    gameObject.GetComponent<MeshRenderer>().material.SetColor("_EmissionColor", Color.green * 0.25f);
                }

                gameObject.GetComponent<MeshRenderer>().enabled = true;
            } 
        }  
    }

    private void OnTriggerExit(Collider other) 
    {
        if(other.name == "VR_Pointer")
        {
           if(gameObject.GetComponent<MeshRenderer>() != null)
           {
               gameObject.GetComponent<MeshRenderer>().enabled = false;
           } 
        }
    }

    bool BoulderFree(Vector3 direction, GameObject opposite)
    {
        bool returnvalue;
        GameObject preventRaycast = transform.parent.GetChild(6).gameObject;

        opposite.SetActive(false);
        preventRaycast.SetActive(false);

        // Check if space behind is free
        Ray ray = new Ray(transform.parent.position, direction);
        RaycastHit hit;

        if(Physics.Raycast(ray, out hit, 2.2f)) //If raycast is colliding with anything in the next 2.2 meters (half a tile from middle + one whole tile - a litte leeway) we dont move
        {
            if(hit.transform.name != "GoalPlaceholder" && hit.transform.name != "GoalArrow")    // Exception for Goal, you can roll rocks on it
            {
                returnvalue = true; // If Goal is blocking, set red
            }
            else    
            {
                returnvalue = false;
            }
        }
        else // If nothing is blocking set green
        {
            returnvalue = false;
        }

        opposite.SetActive(true);                                     // reactivate opposite and prevenraycastthough
        preventRaycast.SetActive(true);
        return returnvalue;
    }

}
