﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class FadeWallClip : MonoBehaviour
{
    float fadeTime = 0.5f;
    
    private void OnTriggerEnter(Collider other) 
    {
        if(other.name != "GoalArrow" && other.name != "GoalPlaceholder" && other.name != "BodyCollider" && other.name != "ARPoVRender")
        {
            Debug.Log("Camera colliding with: " + other.name);
             SteamVR_Fade.Start(Color.black, fadeTime, true);    // Uses the steamvr camera and activates a fade
        }        
    }

    private void OnTriggerExit(Collider other) 
    {
        if(other.name != "GoalArrow" && other.name != "GoalPlaceholder" && other.name != "BodyCollider" && other.name != "ARPoVRender")
        {
            Debug.Log("Camera colliding");
             SteamVR_Fade.Start(Color.clear, fadeTime, true);    // Uses the steamvr camera and deactivates the fade
        }
    }
}
