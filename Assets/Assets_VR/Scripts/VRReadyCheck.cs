﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VRReadyCheck : MonoBehaviour
{   
    [HideInInspector] public bool vrReady = false;

    public void vrIsReady()
    {
        vrReady = true;
    }
}
