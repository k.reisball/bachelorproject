﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class VRReturnFunctionManager : MonoBehaviour
{
    public SteamVR_Action_Boolean returnButtonPressed; // Get Controller Buttonreference
    
    private List<ReturnData> savedReturndata = new List<ReturnData>();   // List of our created structure
    SteamVR_Behaviour_Pose pose = null;
    public static bool isReturning = false;
    float fadeTime = 0.5f;
    SendReturnToAR returnsenderscript = null;
    [SerializeField] GameObject camerarig;
    [SerializeField] RotateAroundTest rotatescript;

    public struct ReturnData        // Create our own Structure that saves the 3 types of data we need
    { 
        public Vector3 teleportPosition;
        public float y_rotation;
        public GameObject interactedObject;
        public Vector3 objectPostion;
        public bool isARObject;

        // Constructer for the struct, so we can create new types of the structure
        public ReturnData(Vector3 teleportPosition, float y_rotation, GameObject interactedObject, Vector3 objectPostion, bool isARObject)
        {
            this.teleportPosition = teleportPosition;
            this.y_rotation = y_rotation;
            this.interactedObject = interactedObject;
            this.objectPostion = objectPostion;
            this.isARObject = isARObject;
        }
    }

    private void Awake() 
    {
        pose = GetComponent<SteamVR_Behaviour_Pose>(); // Accesses the component on the steamVR Controller, to check button input and controller pose
    }

    private void Start() 
    {
        // On start get a reference to the network sender script, which instantiates at awake
        if(GameObject.Find("ReturnSender") != null)
        {
            returnsenderscript = GameObject.Find("ReturnSender").GetComponent<SendReturnToAR>();
        }

        // Add a function that moves the player onto 0/0/0 at start, to prevent him from starting in a wall
        Vector3 groundpos = SteamVR_Render.Top().head.position;
        groundpos.y = camerarig.transform.position.y;
        Vector3 translation = Vector3.zero - groundpos;
        camerarig.transform.position += translation;
        // Also make him face the front from the start
        rotatescript.rotateVRcameraYaxis(-(SteamVR_Render.Top().head.eulerAngles.y));
    }

    // Function to add entries to the list from another script - is being called in ControllerRaycast where all the interactions are happening
    public void addReturnEntry(Vector3 playerTargetPos, float camera_yrot, GameObject interactedObject, Vector3 TargetObjectPos, bool arobject)
    {
        savedReturndata.Add(new ReturnData(playerTargetPos, camera_yrot, interactedObject, TargetObjectPos, arobject));
        Debug.Log("List Entry made");
    }

    // Update is called once per frame
    void Update()
    {
        if(returnButtonPressed.GetStateUp(pose.inputSource) && isReturning == false)    // If Button is pressed & we are not already returning
        {   
            // Check if there is an action entry on the list & we are not in the midst of pushing or teleporting something
            if(savedReturndata.Count.Equals(0) || ControllerRaycast.isPushing == true || ControllerRaycast.isTeleporting == true) 
            {
                return;
            }
            
            // Call the Return Function with the last saved values
            StartCoroutine(ReturnToLast(savedReturndata[savedReturndata.Count - 1].teleportPosition,
                                        savedReturndata[savedReturndata.Count - 1].y_rotation,
                                        savedReturndata[savedReturndata.Count - 1].interactedObject,
                                        savedReturndata[savedReturndata.Count - 1].objectPostion,
                                        savedReturndata[savedReturndata.Count - 1].isARObject
                                        ));

            // Delete the last List entry
            savedReturndata.RemoveAt(savedReturndata.Count - 1);
        }
    }

    IEnumerator ReturnToLast(Vector3 playerTargetPos, float cameratarget_y, GameObject TargetObject, Vector3 TargetObjectPos, bool arobject)
    {
        isReturning = true;
        ToolsUsedCounter.returns++;

        // Send returndata to ar-player too (Position & rotation not needed because it is seperately handled)
        if(returnsenderscript != null)
        {
            returnsenderscript.objectName = TargetObject.name;
            returnsenderscript.objectPos = TargetObjectPos;
            returnsenderscript.arobject = arobject;
        }


        SteamVR_Fade.Start(new Color32(0, 0, 50, 255), fadeTime, true);     // Fade to blue, to indicate difference from teleportation <- Might add custom fade screen later

        yield return new WaitForSeconds(fadeTime);

        // Do player repositiing first, because i will be used on every return-object-scenarios
        // Calculate new CameraRig position, with current HMD position:
        Vector3 head_groundposition = SteamVR_Render.Top().head.position;
        head_groundposition.y = camerarig.transform.position.y;
        // Calculate actual needed translation
        Vector3 translation = playerTargetPos - head_groundposition;
        // move the camerarig
        camerarig.transform.position += translation;

        // Calculate needed rotation x = targetrot - currentrot
        float rotvalue = cameratarget_y - SteamVR_Render.Top().head.eulerAngles.y;
        // Call rotationfunction of the rotationproxy
        rotatescript.rotateVRcameraYaxis(rotvalue);

        if(arobject)
        {
            // Returning an ARObject so destroy it here and reenable arsending on ar-side
            Destroy(TargetObject);
        }
        else
        {
            // Different cases depening on object
            if(TargetObject.tag == "Pushable")                  // Check if Object is a Boulder, not needed for now, but in case we want to reset different interactables too 
            {

                // Move the interacted object
                TargetObject.transform.localPosition = TargetObjectPos;
                TargetObject.transform.rotation = Quaternion.identity;
            }
        }

        SteamVR_Fade.Start(Color.clear, fadeTime, true);

        isReturning = false;
    }
}
