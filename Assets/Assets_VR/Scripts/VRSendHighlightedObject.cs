﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class VRSendHighlightedObject : MonoBehaviourPun, IPunObservable
{
    // the string name will be written to in the controllerRaycast script whenever an object is being highlighted
    [HideInInspector] public string objectToHighlightname = null;
    string lastHighlighted = null;
    VRtoARHighlighter arhighlighterscript = null;

    public void newSceneLoadFunction()
    {
        if(!photonView.IsMine)  // Get a reference to the vrtoar highlighter, when this component is on the ar-client
        {
            arhighlighterscript = GameObject.Find("ARHightlightController").GetComponent<VRtoARHighlighter>();
        }
    }

    // Photonfunction, that is being called twice per update, once from the owner and once from the receiver
    // Only sends data, if the value changed though, as specified in the PhotonView Component of the GameObject
    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if(stream.IsWriting)    // Network owner writes values here
        {
            stream.SendNext(objectToHighlightname);

            // Save the last send data, to check if it changed on new frame
            //lastHighlighted = objectToHighlightname;
            objectToHighlightname = null;
        }
        else    // Network Receiver
        {
            objectToHighlightname = (string)stream.ReceiveNext();
            Debug.Log("Object to highlight received: " + objectToHighlightname);

            // Make sure the received data is new/not null, if it is call ar push function
            if(objectToHighlightname != null)
            {
                // If received data is not null, call ar boulder push
                if(arhighlighterscript == null)
                {
                    newSceneLoadFunction();
                    if (arhighlighterscript == null)
                        return;
                }

                arhighlighterscript.VRtoARHighlight(objectToHighlightname);
            }
        }
    }
}
