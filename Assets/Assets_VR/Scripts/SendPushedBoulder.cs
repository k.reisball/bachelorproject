﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class SendPushedBoulder : MonoBehaviourPun, IPunObservable
{
    // The public data, will be received in the ControllerRaycast script, whenever a boulder-push action is being called
    [HideInInspector] public string BoulderToPush = null;
    [HideInInspector] public Vector3 endposition;
    [HideInInspector] public string rotationDirection = null;
    string lastBoulderToPush;
    Vector3 lastendposition;
    string lastrotationDirection;
    VRtoARBoulderPusher arpushscript = null;

    public void newSceneLoadFunction()
    {
        if(!photonView.IsMine)  // Get a reference to the ar boulder pusher, when this component is on the ar-client
        {
            arpushscript = GameObject.Find("ARNetworkReceiver").GetComponent<VRtoARBoulderPusher>();
        }
    }

    // Photonfunction, that is being called twice per update, once from the owner and once from the receiver
    // Only sends data, if the value changed though, as specified in the PhotonView Component of the GameObject
    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if(stream.IsWriting)    // Network owner writes values here
        {
            stream.SendNext(BoulderToPush);
            stream.SendNext(endposition);
            stream.SendNext(rotationDirection);

            BoulderToPush = null;
        }
        else    // Network Receiver
        {
            BoulderToPush = (string)stream.ReceiveNext();
            endposition = (Vector3)stream.ReceiveNext();
            rotationDirection = (string)stream.ReceiveNext();

            // Make sure the received data is new/not null, if it is call ar push function
            if(BoulderToPush != null)
            {
                Debug.Log("Boulder to push:" + BoulderToPush);
                // If received data is not null, call ar boulder push
                if(arpushscript == null)
                {
                    newSceneLoadFunction();
                    if (arpushscript == null)
                        return;
                }

                arpushscript.PushBoulder(BoulderToPush, endposition, rotationDirection);
            }
        }
    }
}
