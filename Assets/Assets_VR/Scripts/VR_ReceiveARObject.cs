﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class VR_ReceiveARObject : MonoBehaviour
{
    [SerializeField] List<GameObject> ARobjects = new List<GameObject>();
    [SerializeField] VRReturnFunctionManager returnFunctionScript;
    [SerializeField] GameObject camerarig;
    GameObject parent;

    private void Start() 
    {
        parent = GameObject.Find("LevelVR");
    }

    public void placeObject(int objecttype, Vector3 objectpos)
    {
        if(objecttype > 0 && objecttype <= ARobjects.Count) // Make sure received object is of a legit type
        {
            // Instantiate object
            GameObject argo = Instantiate(ARobjects[objecttype-1]);
            // Reparent to level
            argo.transform.SetParent(parent.transform);
            // Set localPos and Rot
            argo.transform.localPosition = objectpos;
            argo.transform.localRotation = Quaternion.identity;

            // Add a Return entry
            // Get the actual player-position, we need to port back to later:
             Vector3 playerposition = SteamVR_Render.Top().head.position;
            playerposition.y = camerarig.transform.position.y;
            returnFunctionScript.addReturnEntry(playerposition, SteamVR_Render.Top().head.eulerAngles.y, argo, Vector3.zero, true);
        }
    }
}
