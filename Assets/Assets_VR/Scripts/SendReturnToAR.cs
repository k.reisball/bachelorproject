﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class SendReturnToAR : MonoBehaviourPun, IPunObservable
{
    // Public Variables being written to from ReturnFunction-script
    [HideInInspector] public string objectName = null;
    [HideInInspector] public Vector3 objectPos;
    [HideInInspector] public bool arobject;
    Vector3 lastPlayerPos;
    string lastObjectName;
    Vector3 lastObjectPos;
    VRAvatarController vrtoarreturnscript = null;

    public void newSceneLoadFunction()
    {
        if(!photonView.IsMine)  // Get a reference to the vrtoar highlighter, when this component is on the ar-client
        {
            vrtoarreturnscript = GameObject.Find("ARNetworkReceiver").GetComponent<VRAvatarController>();
        }
    }

    // Photonfunction, that is being called twice per update, once from the owner and once from the receiver
    // Only sends data, if the value changed though, as specified in the PhotonView Component of the GameObject
    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if(stream.IsWriting)    // Network owner writes values here
        {
            stream.SendNext(objectName);
            stream.SendNext(objectPos);
            stream.SendNext(arobject);

            objectName = null;
        }
        else    // Network Receiver
        {
            objectName = (string)stream.ReceiveNext();
            objectPos = (Vector3)stream.ReceiveNext();
            arobject = (bool)stream.ReceiveNext();

            // Make sure the received data is new/not null, if it is call ar push function
            if(objectName != null)
            {
                // If received data is not null, call ar boulder push
                if(vrtoarreturnscript == null)
                {
                    newSceneLoadFunction();
                    if (vrtoarreturnscript == null)
                        return;
                }

                vrtoarreturnscript.ReturnReceived(objectName, objectPos, arobject);
            }
        }
    }
}
