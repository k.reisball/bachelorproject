﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class VRSendLaserPointer : MonoBehaviourPun
{
    LineRenderer laserInVR = null;
    LineRenderer laserInAR = null;
    Vector3 currentPoint;

    public void newSceneLoadFunction()
    {
        if (photonView.IsMine)  // Get a reference to the vrtoar highlighter, when this component is on the ar-client
        {
            laserInVR = GameObject.Find("LineRenderer").GetComponent<LineRenderer>();
        }
        else
        {
            laserInAR = GameObject.Find("VRLaser").GetComponent<LineRenderer>();
        }
    }

    private void Update()
    {
        if(photonView.IsMine)
        {
            if(laserInVR == null)
            {
                newSceneLoadFunction();
                if(laserInVR == null)
                    return;
            }

            currentPoint = laserInVR.GetPosition(1); // Get current endpoint of VR laser
            currentPoint = laserInVR.transform.TransformPoint(currentPoint); // Transform it into world space
            transform.position = currentPoint; // Set this object to the calculated point
        }
        else
        {
            if (laserInAR == null)
            {
                newSceneLoadFunction();
                if (laserInAR == null)
                    return;
            }

            currentPoint = transform.localPosition; // Get the current position in "scaled-down-world coordinates"
            currentPoint = laserInAR.transform.InverseTransformPoint(currentPoint); // Transform the point into local coordinates of the line renderer

            // Scaling of the laser length still is wrong lets, so get the point at 1/10th of the distance
            Vector3 scaledPoint = laserInAR.transform.position + 0.1f * currentPoint;
            laserInAR.SetPosition(1, scaledPoint); // Set the endposition of the laser to the new point
        }
    }
}
