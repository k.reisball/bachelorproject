﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BodyDontInheritHeadRotation : MonoBehaviour
{
    Vector3 newPos;
    [SerializeField] Transform HeadPos;

    // Update is called once per frame
    void Update()
    {
        // We want to the Bodycollider to stay attatched with the headcollider, but not rotate, so we just inhert it's position manually

        newPos = HeadPos.position;
        newPos.y = newPos.y - 0.694f;     // y-offset for the correct Positioning

        transform.position = newPos;
    }
}
