﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LobbyGoalReady : MonoBehaviour
{
    [SerializeField] public VRReadyCheck vrreadyscrip;
    [SerializeField] GameObject waiting;

    private void OnTriggerEnter(Collider other) {
        if(other.name == "BodyCollider")
        {
            vrreadyscrip.vrReady = true;
            waiting.SetActive(true);
        }
        else
        {
            return;
        }
    }
}
