﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePlayerUp : MonoBehaviour
{
    GameObject player;
    float offset;
    float oldPos;

    // Start is called before the first frame update
    void Start()
    {
        if(GameObject.Find("[CameraRig]") != null)
        {
            player = GameObject.Find("[CameraRig]");
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if(player != null)
        {
            // Calculate Rig to Tile offset
            if (other.name == "BodyCollider")
            {
                offset = player.transform.localPosition.y - transform.parent.localPosition.y;
                oldPos = transform.parent.localPosition.y;
            }
        }
    }

    void OnTriggerStay(Collider other)
    {
        if(player != null)
        {
            if (other.name == "BodyCollider")
            {
                if (oldPos != transform.parent.localPosition.y)
                {
                    oldPos = transform.parent.localPosition.y;
                    Vector3 temp = player.transform.localPosition;
                    float newoffset = temp.y - transform.parent.localPosition.y;
                    float move = offset - newoffset;
                    temp.y += move;
                    player.transform.localPosition = temp;
                }
            }
        }
    }
}
