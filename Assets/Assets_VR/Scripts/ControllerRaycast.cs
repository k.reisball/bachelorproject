﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class ControllerRaycast : MonoBehaviour
{
    // General
    [SerializeField] GameObject pointer;
    [SerializeField] LineRenderer linerenderer;
    SteamVR_Behaviour_Pose pose = null;
    public SteamVR_Action_Boolean touchpadpressed;
    public SteamVR_Action_Boolean triggerpressed;
    int layermask;
    int pointerOver = 0; // 0 = nothing, 1 = teleportable, 2= pushable, 3=interactable
    GameObject currentHover = null;

    // Teleporting
    float fadeTime = 0.5f;
    public static bool isPushing = false;       // might be needed in multiple scripts
    [SerializeField] GameObject camerarig;

    // Highlighting
    [HideInInspector] public bool isHighlighted = false;
    float highlightlerptime = 0.8f; // uses it twice, so lerps 1.6s total
    [HideInInspector] public Coroutine lastCoroutine = null; // Needed to get an Identifier for the current active Highlight so we can stop it if needed
    GameObject lastHighlight = null;
    VRSendHighlightedObject highlightsender = null;
    [SerializeField] ARtoVRHighlighter arHighlighter;
    [SerializeField] GameObject directionArrow;

    // Highlighting through PoV Plane
    [SerializeField] GameObject povplane;
    [SerializeField] Camera povcamera;
    List<Vector3> VerticeList = new List<Vector3>(); //List of global vertices on the plane

    // Pushing
    public static bool isTeleporting = false;   // might be needed in multiple scripts
    string pushdirection = null;
    GameObject boulder;
    float lerptime = 1.5f;
    SendPushedBoulder bouldersender = null;

    // Returning
    [SerializeField] VRReturnFunctionManager returnFunctionScript;

    private void Awake() 
    {
        pose = GetComponent<SteamVR_Behaviour_Pose>(); // Get Controller Reference
        layermask = LayerMask.GetMask("Interactable");
    }

    private void Start() 
    {
        // On start get a reference to the network sender scripts, which instantiates at awake
        if(GameObject.Find("BoulderPushSender") != null)
        {
            bouldersender = GameObject.Find("BoulderPushSender").GetComponent<SendPushedBoulder>();
        }
        
        if(GameObject.Find("VRtoARHighlightSender") != null)
        {
            highlightsender = GameObject.Find("VRtoARHighlightSender").GetComponent<VRSendHighlightedObject>();
        }

        // Get the vertices of the pov plane object
        VerticeList = new List<Vector3>(povplane.GetComponent<MeshFilter>().sharedMesh.vertices);
        // Get ARpov-Cam
        povcamera = GameObject.Find("VRLight").transform.GetChild(0).GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        // Check if Pointerdirection hits a position
        pointerOver = PointerDirection();
        // Activate Pointer if ray is hitting sth
        if(pointerOver == 0){
            pointer.SetActive(false);
            linerenderer.SetPosition(1, new Vector3(0, 0, 10));
        } else
        {
            pointer.SetActive(true);
            linerenderer.SetPosition(1, linerenderer.transform.InverseTransformPoint(pointer.transform.position));
            // Make the Pointer smaller if it is over the PoV Plane
            if (currentHover != null)
            {
                if(currentHover.tag == "PoV")
                {
                pointer.transform.localScale = new Vector3(0.0075f, 0.0075f, 0.0075f); 
                pointer.GetComponent<MeshRenderer>().material.color = Color.magenta;
                }
                else
                {
                    pointer.transform.localScale = new Vector3(0.075f, 0.075f, 0.075f); 
                }
            }
        }


        
        
        // Teleportation
        if(touchpadpressed.GetStateUp(pose.inputSource))
        {
            TryTeleportPush();
        }

        if(triggerpressed.GetStateDown(pose.inputSource))
        {
            TryHighlight();
        }
    }

        private int PointerDirection()
    {
        // Raycast from Controller
        Ray ray = new Ray(transform.position, transform.forward); //Attach to Controller and use forward direction
        RaycastHit hit;
    
        // If it hit something
        if(Physics.Raycast(ray, out hit, 10f, layermask))
        {
            pointer.transform.position = hit.point; //If hitting sth, set pointer to that point
            currentHover = hit.transform.gameObject; // Save currenthoveredgameobject for Highlight
            if(hit.transform.tag == "Teleportable") // if hit is telportable
            {
                pointer.GetComponent<MeshRenderer>().material.color = Color.blue;
                pushdirection = null;
                return 1;
            } else if(hit.transform.tag == "Pushable")
            {
                pointer.GetComponent<MeshRenderer>().material.color = Color.green;
                pushdirection = hit.transform.name;
                boulder = hit.transform.parent.gameObject;
                currentHover = hit.transform.parent.GetChild(0).gameObject; // Need the parents first child for boulder highlights, due to prefab structure
                return 2;
            } else if(hit.transform.tag == "Interactable" || hit.transform.tag == "PoV")
            {
                pointer.GetComponent<MeshRenderer>().material.color = Color.yellow;
                currentHover = hit.transform.gameObject;

                if(hit.transform.parent != null)    // Exception for Boulderobjects, due to its prefab structure
                {
                    if(hit.transform.parent.tag == "Pushable")
                    {
                        currentHover = hit.transform.parent.GetChild(0).gameObject;
                    }
                }

                pushdirection = null;
                return 3;
            } else // if hit is nothing of the above
            {
                pointer.GetComponent<MeshRenderer>().material.color = Color.red;
                pushdirection = null;
                currentHover = null; // If hit nothing relevant current hover is null
                return 0;
            }
        }
        else // If it hit nothing
        {
            pointer.transform.localPosition = new Vector3(-99, -99, -99); // To prevent highlighting with invisible Pointer
            currentHover = null;
            return 0;
        }
    }

        void TryTeleportPush()
    {
        // Check for valid position
        if(isTeleporting || isPushing || VRReturnFunctionManager.isReturning)       // Return if invalid or already telporting or pushing <- Took out isPushing atm, teleportation should be possible while pushing
        {
            return;
        }

        if(pointerOver == 1)
        {
        // Get camerarig and headposition
        Transform cameraRig = camerarig.transform; // Gets SteamVR CameraRig Position
        Vector3 headposition = SteamVR_Render.Top().head.position; // Gets headposition

        // calculate translation
        Vector3 groundPosition = new Vector3(headposition.x, cameraRig.position.y, headposition.z);
        Vector3 translationVector = pointer.transform.position - groundPosition;

        // move
        StartCoroutine(MoveRig(cameraRig, translationVector));
        }


        // Make sure we are not already teleporting, pushing or returning, while trying to push something
        if(isTeleporting || isPushing || VRReturnFunctionManager.isReturning)
        {
            return;
        }

        if(pointerOver == 2)
        {
            // Get Pushdirection depending on raycast side
            Vector3 translationdirection;
            string rotationdirection;
            switch(pushdirection)
            {
                case "Front":
                    translationdirection = Vector3.back;
                    rotationdirection = "xminus";
                    break;
                case "Back":
                    translationdirection = Vector3.forward;
                    rotationdirection = "xplus";
                    break;
                case "Left":
                    translationdirection = Vector3.right;
                    rotationdirection = "zminus";
                    break;
                case "Right":
                    translationdirection = Vector3.left;
                    rotationdirection = "zplus";
                    break;
                default:
                    return;
            }

            // Check if space behind is free
            boulder.gameObject.SetActive(false); // To prevent hitting the colliders of the object that is being pushed
            Ray ray = new Ray(boulder.transform.localPosition, translationdirection);
            RaycastHit hit;
            // Could add Interactable Raycast layer if needed later on
            if(Physics.Raycast(ray, out hit, 2.2f)) //If raycast is colliding with anything in the next 2.2 meters (half a tile from middle + one whole tile - a litte leeway) we dont move
            {
                if(hit.transform.name != "GoalPlaceholder" && hit.transform.name != "GoalArrow")    // Exception for Goal, you can roll rocks on it
                {
                    boulder.SetActive(true);
                    return; 
                }
            }
            // if nothing or only the Goal is blocking the boulder
            boulder.SetActive(true);
            Vector3 targetposition = boulder.gameObject.transform.localPosition + translationdirection*1.5f;
            StartCoroutine(MoveBoulder(boulder, targetposition, rotationdirection));
        }
    }

    private IEnumerator MoveRig(Transform cameraRig, Vector3 translation)
    {
        isTeleporting = true;

        SteamVR_Fade.Start(Color.black, fadeTime, true);    // Uses the steamvr camera and activates a fade
        yield return new WaitForSeconds(fadeTime); // Wait for fade

        cameraRig.position += translation; // Moves player

        SteamVR_Fade.Start(Color.clear, fadeTime, true); // Fades back to clear
        
        isTeleporting = false; // Done teleporting
    }

    private IEnumerator MoveBoulder(GameObject boulderToPush, Vector3 endposition, string rotationdirection)
    {
        isPushing = true;

        // Create a Return Entry
        // Get the actual player-position, we need to port back to later:
        Vector3 playerposition = SteamVR_Render.Top().head.position;
        playerposition.y = camerarig.transform.position.y;
        returnFunctionScript.addReturnEntry(playerposition, SteamVR_Render.Top().head.eulerAngles.y, boulderToPush, boulderToPush.transform.localPosition, false);

        // Send information to network-handler so it gets pushed on ar side too
        if(bouldersender != null)
        {
            bouldersender.BoulderToPush = boulderToPush.name;
            bouldersender.endposition = endposition;
            bouldersender.rotationDirection = rotationdirection;
        }


        // Lerp Position & Rotation of Boulder
        Vector3 startPos = boulderToPush.transform.localPosition;
        Vector3 startRot = boulderToPush.transform.localEulerAngles;
        Vector3 endRot = startRot;
        // Determine Rotationaxis and direction
        switch(rotationdirection)
        {
            case "xminus":
                endRot.x -= 360f;
                break;
            case "xplus":
                endRot.x += 360f;
                break;
            case "zminus":
                endRot.z -= 360f;
                break;
            case "zplus":
                endRot.z += 360f;
                break;
            default:
                break;
        }
        float currentLerpTime = 0f;
        float percent = 0f;

        while(percent < 1)  // Move the object a little bit every frame until destination reached
        {
            currentLerpTime += Time.deltaTime;
            if(currentLerpTime >= lerptime)
            {
                currentLerpTime = lerptime;
            }
            percent = currentLerpTime/lerptime;
            boulderToPush.transform.localPosition = Vector3.Lerp(startPos, endposition, percent);
            boulderToPush.transform.localEulerAngles = Vector3.Lerp(startRot, endRot ,percent);
            yield return null;
        }

        isPushing = false;
    }

    void TryHighlight()
    {
        // Check for valid object
        if(pointerOver == 0 || currentHover == null || isTeleporting || VRReturnFunctionManager.isReturning)       // Return if invalid, or in the middle of telporting/returning to prevent accidental highlights
        {
            return;
        }

        if(isHighlighted)   // If sth is currently highlighted, stop that coroutine, disable the outline and start a new one with the other object
        {
            if(lastHighlight == null || lastHighlight == currentHover)
            {
                return;
            }

            StopCoroutine(lastCoroutine);   // Stop Last Highlight
            
            if(lastHighlight.GetComponent<Outline>() != null)   // And turn last objects Outline off
            {
                lastHighlight.GetComponent<Outline>().enabled = false;
                lastHighlight.GetComponent<Outline>().OutlineWidth = 0f;
            }

            isHighlighted = false;                
        }

        // Check if we are hitting the PoV Plane
                // Check if we are hitting the PoV Plane
        if(currentHover.tag == "PoV")
        {
            // Raycast from Controller
            Ray ray = new Ray(transform.position, transform.forward); //Attach to Controller and use forward direction
            RaycastHit hit;
            if(Physics.Raycast(ray, out hit, Mathf.Infinity, layermask))
            {
                // Since the hit target is a plane we can just take the hit Texture-UV-coordinates and plug them into the ViewportPointToRay function
                Ray ray2 = povcamera.ViewportPointToRay(new Vector3(hit.textureCoord[0], hit.textureCoord[1], 0));
                RaycastHit hit2;
                if(Physics.Raycast(ray2, out hit2, Mathf.Infinity, layermask))
                {
                    currentHover = hit2.transform.gameObject;
                    // Add Highlightexception for Boulders again
                    if(currentHover.transform.parent != null)
                    {
                        if(currentHover.tag == "Pushable")
                            currentHover = currentHover.transform.parent.GetChild(0).gameObject;
                    }
                }
            }
        }

        if(currentHover.GetComponent<Outline>() == null)
        {
            currentHover = currentHover.transform.parent.gameObject;
        }

        if(currentHover.GetComponent<Outline>() != null)
        {
            if (currentHover.GetComponent<Outline>().enabled == true)
            {
                // Is highlighted by AR player, turn his Outline off
                arHighlighter.StopCoroutine(arHighlighter.lastCoroutine);
                directionArrow.SetActive(false);
                arHighlighter.isHighlighted = false;
            }
        }


        // Start VR highlight
       lastCoroutine = StartCoroutine(Highlight(currentHover));
       lastHighlight = currentHover;
    }

    IEnumerator Highlight(GameObject objectTohighlight)
    {
        if(objectTohighlight.transform.GetComponent<Outline>() != null)
        {
            isHighlighted = true;
            ToolsUsedCounter.vr_highlight++;

            // Send VR-Highlightedobject to Network handler, so AR receives it
            if(highlightsender != null)
            {
                highlightsender.objectToHighlightname = objectTohighlight.name;
            }
            
            // Get Outlinescript of hovered object and activate
            Outline outlinescript = objectTohighlight.transform.GetComponent<Outline>();
            outlinescript.enabled = true;
            outlinescript.OutlineColor = Color.yellow;
            outlinescript.OutlineWidth = 0f;

            for(int i = 0; i < 4; i++)
            {
                float currentLerpTime = 0f;
                float percent = 0f;

                while(percent < 1)  // Highlight line go thick
                {
                    currentLerpTime += Time.deltaTime;
                    if(currentLerpTime >= highlightlerptime)
                    {
                        currentLerpTime = highlightlerptime;
                    }
                    percent = currentLerpTime/highlightlerptime;
                    outlinescript.OutlineWidth = Mathf.Lerp(0f, 10f, percent);

                    yield return null;
                }

                currentLerpTime = 0f;
                percent = 0f;

                while(percent < 1)  // Highlight line go thin
                {
                    currentLerpTime += Time.deltaTime;
                    if(currentLerpTime >= highlightlerptime)
                    {
                        currentLerpTime = highlightlerptime;
                    }
                    percent = currentLerpTime/highlightlerptime;
                    outlinescript.OutlineWidth = Mathf.Lerp(10f, 0f, percent);

                    yield return null;
                }

            }

            outlinescript.enabled = false;
            isHighlighted = false;
        }
    }
}
