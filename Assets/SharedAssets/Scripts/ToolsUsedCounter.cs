﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.SceneManagement;

public class ToolsUsedCounter : MonoBehaviour
{
    public static int vr_highlight = 0;
    public static int ar_highlight = 0;
    public static int pov_used = 0;
    public static int vr_request_pov = 0;
    public static int ar_request_pov = 0;
    public static int returns = 0;
    public static float time_needed = 0;
    public static bool levelcleared = false;

    string listPath;
    StreamWriter listfile;

    private void Awake()
    {
        //Listfile, if it doesnt exist
        listPath = Application.dataPath + "/counted_list.csv";
        if (!File.Exists(listPath))
        {
            Debug.Log("File does not exist, so create");
            File.Create(listPath).Close(); // StreamWriter needs to be closed right after creating otherwise it is locked for the subsequent writing
            listfile = new StreamWriter(listPath, true);
            listfile.WriteLine("level,time_needed,returns,vr_highlights,ar_highlights,pov_used,vr_request_pov,ar_request_pov");
            listfile.Close();
        }
    }

    private void Update()
    {
        time_needed += Time.deltaTime;

        if (levelcleared)
        {
            addEntry();
            clearCounters();
            levelcleared = false;
        }
    }

    public void addEntry()
    {
        try
        {
            //Open Stream
            listfile = new StreamWriter(listPath, true);
            //Write in file
            listfile.WriteLine(SceneManager.GetActiveScene().name + "," + time_needed + "," + returns + "," + vr_highlight + "," + ar_highlight + "," + pov_used + "," + vr_request_pov + "," + ar_request_pov);
            //Close Stream
            listfile.Close();
            Debug.Log("Entry added");
        }
        catch
        {
            Debug.Log("Could not save entry");
        }
    }

    public static void clearCounters()
    {
        vr_highlight = 0;
        ar_highlight = 0;
        pov_used = 0;
        vr_request_pov = 0;
        ar_request_pov = 0;
        returns = 0;
        time_needed = 0;
    }


    private void OnDestroy()
    {
        addEntry();
    }

    private void OnDisable()
    {
        addEntry();
    }

}
