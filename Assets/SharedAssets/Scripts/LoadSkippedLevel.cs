﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.SceneManagement;

public class LoadSkippedLevel : MonoBehaviourPun
{
    public int skipsTo;

    void Start()
    {
        // Check on what platform I am
        if (!photonView.IsMine) // <- Android
        {
            Debug.Log("Calling Android Coroutine");
            StartCoroutine(loadAndroidleveldelay());
        }
        else // <- PC, because PC scene instantiated the object
        {
            Debug.Log("Calling PC Coroutine");
            StartCoroutine(loadPCleveldelay());
        }
    }

    IEnumerator loadPCleveldelay()
    {
        // Find parentobject
        GameObject parent = GameObject.Find("VRDependencies");

        // Reparent ARnetwork component
        GameObject.Find("ARNetworkSender(Clone)").transform.SetParent(parent.transform);

        // Reparent VRnetwork component
        GameObject.Find("VRNetworkSender(Clone)").transform.SetParent(parent.transform);

        yield return new WaitForSeconds(0.5f);

        PhotonNetwork.LoadLevel(skipsTo + 1);
    }

    IEnumerator loadAndroidleveldelay()
    {
        // Find parentobject
        GameObject parent = GameObject.Find("ARNetworkDependencies");

        // Reparent ARnetwork component
        GameObject.Find("ARNetworkSender(Clone)").transform.SetParent(parent.transform);

        // Reparent VRnetwork component
        GameObject.Find("VRNetworkSender(Clone)").transform.SetParent(parent.transform);

        // Get VRPlayer and parent it to ARDependencies
        GameObject.Find("VRPlayer").transform.SetParent(parent.transform);

        // Destroy old level
        GameObject temp = GameObject.Find("ARAnchor");
        if (temp.transform.childCount > 0)
        {
            Destroy(temp.transform.GetChild(0).gameObject);
        }

        //Check if PoV-Exchange is still active (no need to do on VR since we are not bringing the objects with us)
        if (GameObject.Find("VRPoVCam") != null)
        {
            GameObject.Find("VRPoVCam").SetActive(false);
        }

        yield return new WaitForSeconds(0.5f);

        PhotonNetwork.LoadLevel(skipsTo);
    }
}
