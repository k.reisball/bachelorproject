﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class SkipsLevels : MonoBehaviourPun
{
    [SerializeField] List<GameObject> skipprefabs = new List<GameObject>();
    
    public void skipButton(int skipTo)
    {
        PhotonNetwork.Instantiate(skipprefabs[skipTo].name, Vector3.zero, Quaternion.identity);
    }
}
