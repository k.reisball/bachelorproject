﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PressurePlateScript : MonoBehaviour
{
    [SerializeField] List<GameObject> goUp = new List<GameObject>();
    [SerializeField] List<bool> isUp = new List<bool>();
    [SerializeField] float tile_startheight;
    [SerializeField] float tile_endheight;

    bool triggered = false;
    float lerptime = 1.4f;

    Vector3 platestartpos;
    Vector3 plateendpos;

    void Start()
    {
        platestartpos = transform.GetChild(0).localPosition;
        plateendpos = platestartpos;
        plateendpos.y -= 0.6f;
    }

    void OnTriggerEnter(Collider other)
    {
        Debug.Log(other.name + " has entered the collider");
        // Check for the objects you want to cll
        if (other.name != "PreventRaycastThrough" && other.name != "BodyCollider")
        {
            Debug.Log("Returning, because name is:" + other.name);
            return;
        }
        Debug.Log("Passed if name, triggered is: " + triggered);

        if(!triggered)
        {
            triggered = true;

            // Start Coroutine to Lerp Plate down
            StartCoroutine(platelerp(true));

            // Check for each list object if boulder above, if not StartCoroutine for it
            if(goUp.Count > 0)
            {
                for(int i = 0; i < goUp.Count; i++)
                {
                    if(!isUp[i]) // If platform is already up, dont send it up again
                    {
                        // Deactivate the two colliders of the tile object first
                        goUp[i].GetComponent<BoxCollider>().enabled = false;
                        goUp[i].transform.GetChild(0).GetComponent<BoxCollider>().enabled = false;

                        Ray ray = new Ray(goUp[i].transform.localPosition, Vector3.up);
                        RaycastHit hit;

                        bool goingUp = true;

                        if (Physics.Raycast(ray, out hit, 3f)) //If raycast is colliding with anything in the next 2.2 meters (half a tile from middle + one whole tile - a litte leeway) we dont move
                        {
                            if (hit.transform.name == "PreventRaycastThrough")   // dont go up if rock is on the tile
                            {
                                goingUp = false;
                            }
                        }

                        if (goingUp)
                        {
                            Debug.Log("Rock is going up");
                            StartCoroutine(objectlerp(goUp[i], true)); // Send it up
                            isUp[i] = true; // Mark it as up
                        }

                        // Reenable collider
                        goUp[i].GetComponent<BoxCollider>().enabled = true;
                        goUp[i].transform.GetChild(0).GetComponent<BoxCollider>().enabled = true;
                    }
                }
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        Debug.Log(other.name + " has exited pressure plate");

        if (other.name != "PreventRaycastThrough" && other.name != "BodyCollider")
        {
            return;
        }

        if (triggered)
        {
            triggered = false;

            // Start Coroutine to Lerp Plate down
            StartCoroutine(platelerp(false));

            // Check for each list object if boulder above, if not StartCoroutine for it
            if (goUp.Count > 0)
            {
                for (int i = 0; i < goUp.Count; i++)
                {
                    if(isUp[i]) // Check if platform is elevated if yes put it down
                    {
                        StartCoroutine(objectlerp(goUp[i], false));
                        isUp[i] = false;
                    }
                }
            }
        }
    }

    IEnumerator platelerp(bool down)
    {
        float currentLerpTime = 0f;
        float percent = 0f;

        if(down)
        {
            while(percent < 1)  // Move the object a little bit every frame until destination reached
            {
                currentLerpTime += Time.deltaTime;
                if(currentLerpTime >= lerptime)
                {
                    currentLerpTime = lerptime;
                }
                percent = currentLerpTime/lerptime;
                transform.GetChild(0).localPosition = Vector3.Lerp(platestartpos, plateendpos, percent);
                yield return null;
            }
        }
        else
        {
             while(percent < 1)  // Move the object a little bit every frame until destination reached
            {
                currentLerpTime += Time.deltaTime;
                if(currentLerpTime >= lerptime)
                {
                    currentLerpTime = lerptime;
                }
                percent = currentLerpTime/lerptime;
                transform.GetChild(0).localPosition = Vector3.Lerp(plateendpos, platestartpos,percent);
                yield return null;
            }  
        }
    }

    IEnumerator objectlerp(GameObject objectToMove, bool up)
    {
        float startpos;
        float endpos;

        if(up)
        {
            startpos = tile_startheight;
            endpos = tile_endheight;
        }
        else
        {   startpos = tile_endheight;
            endpos = tile_startheight;
        }

        float currentLerpTime = 0f;
        float percent = 0f;
        Vector3 currentPos = objectToMove.transform.localPosition;

        while(percent < 1)  // Move the object a little bit every frame until destination reached
        {
            currentLerpTime += Time.deltaTime;
            if(currentLerpTime >= lerptime)
            {
                currentLerpTime = lerptime;
            }
            percent = currentLerpTime/lerptime;
            currentPos.y = Mathf.Lerp(startpos, endpos, percent);

            objectToMove.transform.localPosition = currentPos;

            yield return null;
        }
    }
}
