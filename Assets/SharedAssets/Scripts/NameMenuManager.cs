﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.UI;

public class NameMenuManager : MonoBehaviourPunCallbacks
{
    [SerializeField] GameObject connectscreen;  
    bool isAndroid = false;

    private void Awake() 
    {
        DontDestroyOnLoad(gameObject);
        PhotonNetwork.ConnectUsingSettings();

        if(Application.platform == RuntimePlatform.Android)
        {
            isAndroid = true;
        }
    }

    public override void OnConnectedToMaster()  // Called when connected to Master
    {
        Debug.Log("Connected with Master");
        PhotonNetwork.JoinLobby(TypedLobby.Default);  // Make a Lobby
    }

    public override void OnJoinedLobby()    // Called when connected to Lobby
    {
        Debug.Log("Connected with Lobby");
        connectscreen.SetActive(true);     // Enables Name Entering Field

        if(isAndroid)
        {
            PhotonNetwork.NickName = "ARPlayer"; //assigns entered name to PhotonNetwork
        } else
        {
            PhotonNetwork.NickName = "VRPlayer";
        }
    }

        public void OnClickCreateRoom() // Creates a room
    {
        PhotonNetwork.JoinOrCreateRoom("room1", new RoomOptions{MaxPlayers=2}, TypedLobby.Default); // Creates room with max 2 player limit
    }

    public override void OnJoinedRoom() // Is being called when user joins a room
    {
        if(isAndroid)
        {
            PhotonNetwork.LoadLevel(1); // Loads Unity Scene level according to Build Index - AR Scene
        }
        else
        {
            PhotonNetwork.LoadLevel(2); // Loads Unity Scene level according to Build Index - VR Scene
        }       
    }
}
