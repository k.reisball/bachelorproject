﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class ARSendHighlighted : MonoBehaviourPun, IPunObservable
{
    // Works analogue to VRtoARHighlightsender

    // the string name will be written to in the controllerRaycast script whenever an object is being highlighted
    [HideInInspector] public string objectToHighlightname = null;
    string lastHighlighted = null;
    ARtoVRHighlighter vrhighlighterscript = null;

    public void newSceneLoadFunction()
    {
        if(!photonView.IsMine)  // Get a reference to the vrtoar highlighter, when this component is on the vr-client
        {
            vrhighlighterscript = GameObject.Find("VRNetworkReceiver").GetComponent<ARtoVRHighlighter>();
        }
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if(stream.IsWriting)    // Network owner writes values here
        {
            stream.SendNext(objectToHighlightname);

            // Save the last send data, to check if it changed on new frame
            //lastHighlighted = objectToHighlightname;
            objectToHighlightname = null;
        }
        else    // Network Receiver
        {
            objectToHighlightname = (string)stream.ReceiveNext();
            Debug.Log("Object to highlight received: " + objectToHighlightname);

            // Make sure the received data is new/not null, if it is call ar push function
            if (objectToHighlightname == null)
            {
                newSceneLoadFunction();
                if (objectToHighlightname == null)
                    return;
            }

            // If received data is not null, call ar boulder push
            if(vrhighlighterscript != null)
            {
                vrhighlighterscript.ARtoVRHighlight(objectToHighlightname);
            }
        }
    }
}
