﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class ARReadySender : MonoBehaviourPun, IPunObservable
{
    ARReadyCheck arReadyScript;
    VRReadyCheck vrReadyScript;
    bool otherPlayerReady = false;
    bool oneCoroutine = false;

    private void Awake() 
    {
        Debug.Log("Awake called");
        if(photonView.IsMine)
        {  
            arReadyScript = GameObject.Find("Intro").GetComponent<ARReadyCheck>();
        }
        else
        {
            vrReadyScript =  GameObject.Find("Intro").GetComponent<VRReadyCheck>();
        }    
    }

    private void Update() 
    {
        if(otherPlayerReady && vrReadyScript.vrReady)
        {
            if(oneCoroutine == false)
            {
                StartCoroutine(loadScenewithDelay());
            }
        }    
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if(stream.IsWriting)    // Network owner writes values here
        {
            stream.SendNext(arReadyScript.arReady);
        }
        else    // Network Receiver
        {
            otherPlayerReady = (bool)stream.ReceiveNext();
        }
    }

    IEnumerator loadScenewithDelay()
    {
        oneCoroutine = true;

        yield return new WaitForSeconds(1f);    // In case of lag

        // Find parentobject
        GameObject parent = GameObject.Find("VRDependencies");

        // Reparent ARnetwork component
        GameObject.Find("ARNetworkSender(Clone)").transform.SetParent(parent.transform);

        // Reparent VRnetwork component
        GameObject.Find("VRNetworkSender(Clone)").transform.SetParent(parent.transform);

        yield return new WaitForSeconds(1f);    // In case of lag

        // Load new VR Level
        PhotonNetwork.LoadLevel(4); // Loads Unity Scene level according to Build Index - AR Level 1

        oneCoroutine = false;
    }
}
