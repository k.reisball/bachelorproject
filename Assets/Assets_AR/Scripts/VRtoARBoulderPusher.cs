﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VRtoARBoulderPusher : MonoBehaviour
{
    float lerptime = 1.5f;

    public void PushBoulder(string objectName, Vector3 endPosition, string rotationDirection)
    {
        GameObject temp = GameObject.Find(objectName);
        // Change to GameObjects.Find & check for the AR layer here, if fusing scenes

        if(temp != null)
        {
            StartCoroutine(Pushing(temp, endPosition, rotationDirection));
        }
    }

    IEnumerator Pushing (GameObject boulder, Vector3 targetPosition, string rotationdirection)
    {
        Debug.Log("AR Pushing called");
        // Will not check for multiple pushing boulders at the same time for now, to give some leniency for network lag
        // will add if needed later. Trying to prevent multiple pushes, by passing the targetPosition

        // Lerp Position & Rotation of Boulder
        Vector3 startPos = boulder.transform.localPosition;
        Vector3 startRot = boulder.transform.localEulerAngles;
        Vector3 endRot = startRot;
        // Determine Rotationaxis and direction
        switch(rotationdirection)
        {
            case "xminus":
                endRot.x -= 360f;
                break;
            case "xplus":
                endRot.x += 360f;
                break;
            case "zminus":
                endRot.z -= 360f;
                break;
            case "zplus":
                endRot.z += 360f;
                break;
            default:
                break;
        }
        float currentLerpTime = 0f;
        float percent = 0f;

        while(percent < 1)  // Move the object a little bit every frame until destination reached
        {
            currentLerpTime += Time.deltaTime;
            if(currentLerpTime >= lerptime)
            {
                currentLerpTime = lerptime;
            }
            percent = currentLerpTime/lerptime;
            boulder.transform.localPosition = Vector3.Lerp(startPos, targetPosition, percent);
            boulder.transform.localEulerAngles = Vector3.Lerp(startRot, endRot ,percent);
            yield return null;
        }
        Debug.Log("AR Pushing finished");
    }
}
