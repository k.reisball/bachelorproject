﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using TMPro;

public class ARSendPoV : MonoBehaviourPun, IPunObservable
{
    public bool ARstartSending = false;
    VRPoVSend vrbool = null;
    GameObject VRPoVScreen = null;
    GameObject VRPoVCamera = null;
    GameObject requestedaid = null;
    bool onlyOneCoroutineA = false;
    bool onlyOneCoroutineB = false;
    bool onlyOneCoroutineC = false;
    bool onlyOneCoroutineD = false;
    GameObject requestSent = null;
    TextMeshProUGUI ARrequestaid = null;
    GameObject ARPoVScreen = null;

    public void newSceneLoadFunction()
    {
        if(!photonView.IsMine)  // Get a reference to VR-SendPoVBool, VR-PoV Screen & VR-PoVCamera
        {
            vrbool = GameObject.Find("VRPoVSender").GetComponent<VRPoVSend>();
            VRPoVScreen = GameObject.Find("ARPoVDisplay").transform.GetChild(0).gameObject;
            VRPoVCamera = GameObject.Find("VRLight").transform.GetChild(0).gameObject;
            requestedaid = GameObject.Find("PoVRequested").transform.GetChild(0).gameObject;
            requestSent = GameObject.Find("PoVRequested").transform.GetChild(1).gameObject;
        }
        else
        {
            ARrequestaid = GameObject.Find("ARRequest").transform.GetChild(0).GetChild(0).GetComponent<TextMeshProUGUI>();
            ARPoVScreen = GameObject.Find("Head").transform.GetChild(0).gameObject;
            if(ARrequestaid != null)
            {
                // Reset Button text, in case new level got loaded while requested
                ARrequestaid.text = "Request perspective exchange";
                ARrequestaid.color = Color.white;
            }
        }

        // Reset coroutine bools, in case level got loaded mid-coroutine
        onlyOneCoroutineA = false;
        onlyOneCoroutineB = false;
        onlyOneCoroutineC = false;
        onlyOneCoroutineD = false;
        ARstartSending = false;
    }

    private void Update() // Bad practise, is for testing
    {
        if(!photonView.IsMine)
        {
            if (vrbool == null || VRPoVScreen == null || VRPoVCamera == null || requestedaid == null || requestSent == null)
                newSceneLoadFunction();
        }
        else
        {
            if (ARrequestaid == null || ARPoVScreen == null)
                newSceneLoadFunction();
        }
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if(stream.IsWriting)    // Network owner writes values here
        {
            stream.SendNext(ARstartSending);

            if(onlyOneCoroutineA == false && ARstartSending == true)
            {
                StartCoroutine(DelayedFalse());
            }
        }
        else    // Network Receiver
        {
            ARstartSending = (bool)stream.ReceiveNext();

            if(ARstartSending == true)
            {
                if(vrbool.VRstartSending == true)
                {
                    Debug.Log("Calling RPC AR");
                    ToolsUsedCounter.vr_request_pov++;
                    photonView.RPC("BothConfirmed", RpcTarget.All);
                }
                else
                {
                    ToolsUsedCounter.ar_request_pov++;
                    StartCoroutine(Requested());
                }
            }
        }
    }

    // Method that gets called on all clients
    [PunRPC]
    void BothConfirmed(PhotonMessageInfo info)
    {
        ToolsUsedCounter.pov_used++;
        Debug.Log("RPC called AR");
        if(photonView.IsMine == true)
        {
            if(onlyOneCoroutineD == false)
            {
                StartCoroutine(StartPoVexchangeAR());
            }
        }
        else
        {
            if(onlyOneCoroutineB == false)
            {
                StartCoroutine(StartPoVexchangeVR());
            }
        }
    }

    IEnumerator DelayedFalse()
    {
        onlyOneCoroutineA = true;

        if(ARrequestaid != null)
        {
            ARrequestaid.text = "Other player needs to confirm...";
            ARrequestaid.color = Color.green;
        }

        // Give the other player 10 seconds to respond
        yield return new WaitForSeconds(10f);

        if(ARrequestaid != null)
        {
            ARrequestaid.text = "Request perspective exchange";
            ARrequestaid.color = Color.white;
        }

        ARstartSending = false;

        onlyOneCoroutineA = false;
    }

    IEnumerator StartPoVexchangeAR()
    {
        onlyOneCoroutineD = true;

        if(ARPoVScreen != null)
        {
            if(ARrequestaid != null)
            {
                ARrequestaid.text = "Request perspective exchange";
                ARrequestaid.color = Color.white;
            }

            ARPoVScreen.SetActive(true);

            yield return new WaitForSeconds(15f);

            ARPoVScreen.SetActive(false);
        }

        onlyOneCoroutineD = false;
    }

    IEnumerator StartPoVexchangeVR()
    {
        onlyOneCoroutineB = true;

        if(VRPoVScreen != null && VRPoVCamera != null) 
        {
            VRPoVCamera.SetActive(true);
            VRPoVScreen.SetActive(true);

            if(requestedaid != null)
            {
                requestedaid.SetActive(false);
            }

            if(requestSent != null)
            {
                requestSent.SetActive(false);
            }

            yield return new WaitForSeconds(15f);

            VRPoVCamera.SetActive(false);
            VRPoVScreen.SetActive(false);
        }

        onlyOneCoroutineB = false;
    }

    IEnumerator Requested()
    {
        onlyOneCoroutineC = true;

        if(requestedaid != null)
        {
            requestedaid.SetActive(true);

            // Activate only 7.5 sec to preft confusion due to lag
            yield return new WaitForSeconds(7.5f);

            requestedaid.SetActive(false);
        }

        onlyOneCoroutineC = false;
    }
}
