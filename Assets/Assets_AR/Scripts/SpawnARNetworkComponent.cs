﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class SpawnARNetworkComponent : MonoBehaviourPun
{
    [SerializeField] GameObject ARLobbyReadySender;
    [SerializeField] GameObject ARNetworkComponent;

    private void Awake() 
    {
        // Instantiate the VR Network-Player Proxy
        GameObject ARready = PhotonNetwork.Instantiate(ARLobbyReadySender.name, Vector3.zero, Quaternion.identity);
        GameObject ARproxy = PhotonNetwork.Instantiate(ARNetworkComponent.name, Vector3.zero, Quaternion.identity);
    }
}
