﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Photon.Pun;

public class ARLightPosSender : MonoBehaviourPun
{
    GameObject ARCamera = null;
    GameObject VRLight = null;
    [SerializeField] ARSendHighlighted sendARHighlightscript;
    [SerializeField] ARSendPoV sendARPoVscript;
    [SerializeField] ARObjectSender arobjectsenderscript;
    bool camAndLvlFound = false;

    private void Start() 
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        Debug.Log("ARNetwork Scene loaded Triggered");
        // Call newSceneLoadfunction of this object and all children objects (except for lobby-ready-checker)
        newSceneLoadFunction();
        sendARHighlightscript.newSceneLoadFunction();
        sendARPoVscript.newSceneLoadFunction();
        arobjectsenderscript.newSceneLoadFunction();
    }

    void newSceneLoadFunction()
    {
        // Find the right objects in respective scene and reparent this proxy-object to the level so we can use local-transforms
        if(photonView.IsMine)
        {
            ARCamera = GameObject.Find("ARCamera");
            transform.SetParent(GameObject.Find("LevelAR").transform);
        }
        else
        {
            VRLight = GameObject.Find("VRLight");
            transform.SetParent(GameObject.Find("LevelVR").transform);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(photonView.IsMine)
        {
            // I am the AR Scene

            if(!camAndLvlFound)
            {
               if(GameObject.Find("LevelAR").transform != null)
               {
                   if (ARCamera == null)
                   {
                       newSceneLoadFunction();
                        if (ARCamera == null)
                        {
                            return;
                        }
                        else
                        {
                            transform.position = ARCamera.transform.position;
                            transform.rotation = ARCamera.transform.rotation;
                            camAndLvlFound = true;
                        }
                   }                        
               }
            }
            else
            {
                transform.position = ARCamera.transform.position;
                transform.rotation = ARCamera.transform.rotation;
            }

        } else
        {
            // I am the VR Scene
            if(VRLight == null)
            {
                newSceneLoadFunction();
                if (VRLight == null)
                    return;
            }

            VRLight.transform.localPosition = transform.localPosition;
            VRLight.transform.localRotation = transform.localRotation;
        }
    }
}
