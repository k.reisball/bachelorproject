﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VRtoARHighlighter : MonoBehaviour
{
    // Same script as the ARtoVRHighlighter, just with a different color 

    float lerptime = 0.8f;
    [HideInInspector] public bool isHighlighted = false;
    [HideInInspector] public Coroutine lastCoroutine = null; // Needed to get an Identifier for the current active Highlight so we can stop it if needed
    GameObject lastHighlight = null;
    [SerializeField] ObjectHighlighting arHighlighter;

    public void VRtoARHighlight(string objectToHighlight)
    {
        Debug.Log("Highlighter being called with: " + objectToHighlight);
        GameObject temp = GameObject.Find(objectToHighlight);   // Find corresponding object in my Scene
        // If scenes are fused add a check for AR or VR visible Layer here
        if(temp != null)
        {
            if(!isHighlighted)  // Nothing being highlighted by VR Player
            {
                if(temp.GetComponent<Outline>() != null)
                {
                    if(temp.GetComponent<Outline>().enabled == true)
                    {
                        // Outlined by AR player, stop his outline
                        arHighlighter.StopCoroutine(arHighlighter.lastCoroutine);
                        arHighlighter.isHighlighted = false;
                    }
                }

                // Start VR Highlight
                lastCoroutine = StartCoroutine(VRHighlighted(temp));
                lastHighlight = temp;
            }
            else    // Something is being highlighted
            {
                if(lastHighlight == temp)   // Forbids rehighlighting the same object while it is being highlighted, but allows it again when highlight is done
                {
                    return;
                }

                StopCoroutine(lastCoroutine);   // Stop Last Highlight

                if(lastHighlight.GetComponent<Outline>() != null)   // And turn last objects Outline off
                {
                    lastHighlight.GetComponent<Outline>().enabled = false;
                    lastHighlight.GetComponent<Outline>().OutlineWidth = 0f;
                }

                isHighlighted = false;       

                lastCoroutine = StartCoroutine(VRHighlighted(temp));   // Restart Highlighting with new Object
                lastHighlight = temp;
            }
        }
    }

    IEnumerator VRHighlighted(GameObject highlightThis)
    {
        // Not checking if the Object is already highlight, so overwriting each others highlights is possible and wanted behaviour

        if(highlightThis.transform.GetComponent<Outline>() != null)
        {
            isHighlighted = true;

            // Get Outlinescript of hovered object and activate
            Outline outlinescript = highlightThis.transform.GetComponent<Outline>();
            outlinescript.enabled = true;
            outlinescript.OutlineColor = Color.yellow;
            outlinescript.OutlineWidth = 0f;

            for(int i = 0; i < 4; i++)
            {
                float currentLerpTime = 0f;
                float percent = 0f;

                while(percent < 1)  // Same lerping as for pushing object
                {
                    currentLerpTime += Time.deltaTime;
                    if(currentLerpTime >= lerptime)
                    {
                        currentLerpTime = lerptime;
                    }
                    percent = currentLerpTime/lerptime;
                    outlinescript.OutlineWidth = Mathf.Lerp(0f, 10f, percent);
                    yield return null;
                }

                currentLerpTime = 0f;
                percent = 0f;

                while(percent < 1)  
                {
                    currentLerpTime += Time.deltaTime;
                    if(currentLerpTime >= lerptime)
                    {
                        currentLerpTime = lerptime;
                    }
                    percent = currentLerpTime/lerptime;
                    outlinescript.OutlineWidth = Mathf.Lerp(10f, 0f, percent);
                    yield return null;
                }

            }

            outlinescript.enabled = false;
            isHighlighted = false;
        }
    }
}
