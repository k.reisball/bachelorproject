﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using GoogleARCore;


public class ObjectHighlighting : MonoBehaviour
{
    Camera cam;
    int layer_mask;
    [HideInInspector] public bool isHighlighted = false;
    float lerptime = 0.8f;
    [HideInInspector] public Coroutine lastCoroutine = null; // Needed to get an Identifier for the current active Highlight so we can stop it if needed
    GameObject lastHighlight = null;
    ARSendHighlighted arHighlightsender = null;
    RepositionButton reposition = null;
    [SerializeField] VRtoARHighlighter vrHighlight;

    void Start() 
    {
        layer_mask = LayerMask.GetMask("Interactable"); //Presave Layermask for Raycasting against interactable objects only

        cam = GameObject.Find("ARCamera").GetComponent<Camera>();

        if(GameObject.Find("RepositionCanvas") != null)
        {
            reposition = GameObject.Find("RepositionCanvas").transform.GetChild(0).GetComponent<RepositionButton>();
        }

        // Get a reference to the ARHighlightsender, to send highlighted objects to vr
        if( GameObject.Find("ARtoVRHighlightSender") != null)
        {
            arHighlightsender = GameObject.Find("ARtoVRHighlightSender").GetComponent<ARSendHighlighted>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.touchCount < 1)
        {
            return;
        }

        if(reposition != null)
        {
            if(reposition.repositionAllowed == true)
            {
                return;
            }
        }

        if (IsPointerOverUIObject())      
        {
            return;
        }

        Touch touch = Input.GetTouch(0);    // Get first touch

        if(touch.phase == TouchPhase.Began)  // When the touch just happend
        {
            // Create Ray from Cam through clicked screenpoint
            RaycastHit hit;
            Ray ray = cam.ScreenPointToRay(touch.position); 
            if (Physics.Raycast(ray, out hit, Mathf.Infinity, layer_mask))
            {
                GameObject toHighlight = hit.transform.gameObject;

                // Exception for Boulders, due to their prefab structure
                if(toHighlight.transform.parent != null && toHighlight.transform.parent.tag == "Pushable")
                {
                    toHighlight = toHighlight.transform.parent.GetChild(0).gameObject;
                } 

                // Exception for Tall-Tiles
                if(toHighlight.GetComponent<Outline>() == null)
                {
                    toHighlight = toHighlight.transform.parent.gameObject;
                }

                if(!isHighlighted)  // if nothing is being highlighted by AR player
                {
                    if(toHighlight.GetComponent<Outline>() != null)
                    {
                        if(toHighlight.GetComponent<Outline>().enabled == true)
                        {
                            // is highlighted by vr player, turn his highlight off
                            vrHighlight.StopCoroutine(vrHighlight.lastCoroutine);
                            vrHighlight.isHighlighted = false;
                        }
                    }

                    // turn ar highlight on
                    lastCoroutine = StartCoroutine(HighlightAR(toHighlight));
                    lastHighlight = toHighlight;
                }
                else            // Something is being highlighted
                {
                    if(lastHighlight == null || lastHighlight == toHighlight)
                    {
                        return;
                    }

                    StopCoroutine(lastCoroutine);   // Stop Last Highlight
            
                    if(lastHighlight.GetComponent<Outline>() != null)   // And turn last objects Outline off
                    {
                        lastHighlight.GetComponent<Outline>().enabled = false;
                        lastHighlight.GetComponent<Outline>().OutlineWidth = 0f;
                    }

                    isHighlighted = false;       

                    lastCoroutine = StartCoroutine(HighlightAR(toHighlight));   // Restart Highlighting with new Object
                    lastHighlight = toHighlight;         
                }
            }
        }
    }


        private bool IsPointerOverUIObject()   // Checks if Input is on UI Element
    {
        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        return results.Count > 0;
    }

    IEnumerator HighlightAR(GameObject objectTohighlight)
    {
        if(objectTohighlight.transform.GetComponent<Outline>() != null)
        {
            isHighlighted = true;

            // Send object name to network manager
            if(arHighlightsender != null)
            {
                arHighlightsender.objectToHighlightname = objectTohighlight.name;
            }

            // Get Outlinescript of hovered object and activate
            Outline outlinescript = objectTohighlight.transform.GetComponent<Outline>();
            outlinescript.enabled = true;
            outlinescript.OutlineColor = Color.red;
            outlinescript.OutlineWidth = 0f;

            for(int i = 0; i < 4; i++)
            {
                float currentLerpTime = 0f;
                float percent = 0f;

                while(percent < 1)  // Same lerping as for pushing object
                {
                    currentLerpTime += Time.deltaTime;
                    if(currentLerpTime >= lerptime)
                    {
                        currentLerpTime = lerptime;
                    }
                    percent = currentLerpTime/lerptime;
                    outlinescript.OutlineWidth = Mathf.Lerp(0f, 10f, percent);
                    yield return null;
                }

                currentLerpTime = 0f;
                percent = 0f;

                while(percent < 1)  
                {
                    currentLerpTime += Time.deltaTime;
                    if(currentLerpTime >= lerptime)
                    {
                        currentLerpTime = lerptime;
                    }
                    percent = currentLerpTime/lerptime;
                    outlinescript.OutlineWidth = Mathf.Lerp(10f, 0f, percent);
                    yield return null;
                }

            }

            outlinescript.enabled = false;
            isHighlighted = false;
        }
    }
}
