﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnapARObject : MonoBehaviour
{
    [SerializeField] List<GameObject> arobjects = new List<GameObject>();
    int counter = 0;    // Have a counter for every object that is colliding with the space and prevent placing objects in the hitbox when any object is in it
    GameObject spawnedObject = null;
    GameObject triggerObject = null;
    PublicAndGlobalARVariables arobjectset;
    GameObject parent;
    GameObject sendButton;

    private void Start() 
    {
        arobjectset = GameObject.Find("ReloadARSessionOnScene").GetComponent<PublicAndGlobalARVariables>();
        parent = GameObject.Find("LevelAR");
        sendButton = GameObject.Find("SendAR").transform.GetChild(0).gameObject;
    }

    private void OnTriggerEnter(Collider other) 
    {
        Debug.Log("Is colliding with: " + other.gameObject.name);
        if(counter == 0 && other.gameObject.layer == 17 && !arobjectset.arObjectPlaced && !arobjectset.arObjectSent)
        {
            // Spawn a clone of the marker object here
            for(int i = 0; i < arobjects.Count; i++)
            {
                if(other.gameObject.name == arobjects[i].name)
                {
                    spawnedObject = Instantiate(arobjects[i], transform.position, Quaternion.identity);
                    // Set this object as the parent of the AR object so it moves with the level
                    spawnedObject.transform.SetParent(parent.transform);
                }
            }

            // disable the markerobject renderer
            triggerObject = other.gameObject;
            triggerObject.GetComponent<MeshRenderer>().enabled = false;

            // Only allow one AR object per level
            arobjectset.arObjectPlaced = true;

            // Write Object to sendbuttonscript and activate sendbutton
            sendButton.SetActive(true);
            sendButton.transform.parent.GetComponent<SendObjectButton>().objectToSend = spawnedObject;
        }

        // Increase counter if object is not a pressure plate
        if(other.name != "PressurePlatePressurePlate")
            counter++;
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.name != "PressurePlatePressurePlate")
            counter--;

        if(counter < 0)
            counter = 0;

        if(other.gameObject.layer == 17)
        {
            // destroy the spawned clone
            if(spawnedObject != null)
                Destroy(spawnedObject);

            // enable the markerobject renderer
            if(triggerObject != null)
            {
                triggerObject.GetComponent<MeshRenderer>().enabled = true;
            }

            // Reallow an AR object since this one is not placed anymore
            arobjectset.arObjectPlaced = false;

            // Rewrite Object to sendbuttonscript and deactivate sendbutton
            sendButton.SetActive(false);
            sendButton.transform.parent.GetComponent<SendObjectButton>().objectToSend = null;
        }
    }
}
