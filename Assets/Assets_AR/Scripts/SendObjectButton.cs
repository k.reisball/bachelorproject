﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SendObjectButton : MonoBehaviour
{
    [HideInInspector] public GameObject objectToSend = null;
    [SerializeField] List<GameObject> arobjects = new List<GameObject>();
    ARObjectSender objectSendNetwork;
    GameObject parent;
    GameObject arObjRestriction;

    private void Start()
    {
        if(GameObject.Find("ARSendObject") != null)
            objectSendNetwork = GameObject.Find("ARSendObject").GetComponent<ARObjectSender>();
        
        parent = GameObject.Find("LevelAR");
        arObjRestriction = GameObject.Find("ReloadARSessionOnScene");
    }

    public void SendObject()
    {
        if(objectToSend == null)      
            return;
        

        if(objectSendNetwork == null)
        {
            if (GameObject.Find("ARSendObject") != null)
                objectSendNetwork = GameObject.Find("ARSendObject").GetComponent<ARObjectSender>();

            if (objectSendNetwork == null)
                return;
        }

        int objectType = 0;

        if(objectToSend.tag == "Pushable")
            objectType = 1;

        if(objectType != 0)
        {
            objectSendNetwork.objectToSend = objectType;
            objectSendNetwork.objectPos = objectToSend.transform.localPosition;

            // Also replace the object on our side
            GameObject temp = Instantiate(arobjects[objectType-1]);
            temp.transform.SetParent(parent.transform);
            temp.transform.localPosition = objectToSend.transform.localPosition;
            temp.transform.localRotation = Quaternion.identity;
            temp.transform.localScale = objectToSend.transform.localScale;

            // Hide the Send button after sth has been sent
            transform.GetChild(0).gameObject.SetActive(false);
            
            // Move objecttosend before destroying to make sure collider counter works
            StartCoroutine(WaitForPhysics());
        }
    }

    IEnumerator WaitForPhysics()
    {
        objectToSend.transform.position = Vector3.zero;
        yield return new WaitForFixedUpdate();
        Destroy(objectToSend);

        arObjRestriction.GetComponent<PublicAndGlobalARVariables>().arObjectSent = true;
    }
}
