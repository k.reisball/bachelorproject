﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class ReparentARObject : MonoBehaviourPun
{

    // Start is called before the first frame update
    void Awake()
    {   
        // Reparent the level to the anchor
        GameObject parent = GameObject.Find("ARAnchor");
 
        transform.SetParent(parent.transform);
        transform.localPosition = Vector3.zero;
        transform.localScale = Vector3.one;
        transform.localRotation = Quaternion.identity;

        // Reparent the VR Avatar we kept through the scene
        GameObject vrplayer = GameObject.Find("VRPlayer");
        vrplayer.transform.SetParent(transform);
        vrplayer.transform.localPosition = Vector3.zero;
        vrplayer.transform.localRotation = Quaternion.identity;
    }
}
