﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ARReadyCheck : MonoBehaviour
{
    [HideInInspector] public bool arReady = false;
    [SerializeField] TextMeshProUGUI readybuttontext;

    public void readyButton()
    {
        arReady = true;
        readybuttontext.text = "Waiting for VR-Player...";
    }
}
