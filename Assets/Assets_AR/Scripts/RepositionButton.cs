﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class RepositionButton : MonoBehaviour
{   
    // Allows Repositioning AR Object when true (called in HelloARController)
    [HideInInspector] public bool repositionAllowed = false;
    [SerializeField] TextMeshProUGUI buttonText;
    [SerializeField] GameObject arPointCloud;
    public static bool arHelpOn = true;

    public void repositionChange()
    {
        Debug.Log("Button clicked");
        if(repositionAllowed == false)
        {
            repositionAllowed = true;
            buttonText.text = "Reposition: On";

            enableARhelper();
        } else 
        {
            repositionAllowed = false;
            buttonText.text = "Reposition: Off";

            disableARhelper();
            Debug.Log("Button clicked");
        }
    }

    // Disable function is public since we want to call it from the positioning script, once when the AR object is placed
    public void disableARhelper()
    {
        // Variable that tells ARCore's DetectedPlaneVisualizer to stop enabling the planes mesh-renderers and instead disable them
        arHelpOn = false;

        // Disable pointcloud
        arPointCloud.SetActive(false);
    }

        public void enableARhelper()
    {
        // Do the opposite of disable
        arHelpOn = true;
        arPointCloud.SetActive(true);
    }
}
