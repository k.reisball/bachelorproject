﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeactivateOnStart : MonoBehaviour
{
    // This preloads the 3D asset on App-Start, which makes it start slowly, but does not cause any lag when trying to spawn the Object to the plane! :)
    void Start()
    {
        DontDestroyOnLoad(gameObject);
        gameObject.SetActive(false);
    }
}
