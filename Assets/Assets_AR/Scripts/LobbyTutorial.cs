﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LobbyTutorial : MonoBehaviour
{
    [SerializeField] List<GameObject> tutorialslides = new List<GameObject>();
    int currentslide = 0;
    bool timerover = true;
 
    public void nextSlide()
    {
        if(timerover)
        {
            tutorialslides[currentslide].SetActive(false);
            currentslide++;
            tutorialslides[currentslide].SetActive(true);
            StartCoroutine(ShortDelay());
        }
    }

    IEnumerator ShortDelay()
    {
        timerover = false;
        yield return new WaitForSeconds(0.5f);
        timerover = true;
    }
}
