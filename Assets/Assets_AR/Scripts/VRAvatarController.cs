﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VRAvatarController : MonoBehaviour
{
    [SerializeField] GameObject blueFade;
    float lerptime = 0.5f;

    public void ReturnReceived(string interactedObject, Vector3 objectPosition, bool arobject)
    {
        // Player position is not needed, because it is being actively handled every frame by a photon transform view
        GameObject temp = GameObject.Find(interactedObject);

        if(temp != null)
        {
            StartCoroutine(FadeBlueReturn(temp, objectPosition, arobject));
        }
    }

    IEnumerator FadeBlueReturn(GameObject objectToChange, Vector3 objectOldPos, bool isArObject)
    {
        // Fade to Blue
        float currentLerpTime = 0f;
        float percent = 0f;

        blueFade.SetActive(true);

        while(percent < 1)  // Move the object a little bit every frame until destination reached
        {
            currentLerpTime += Time.deltaTime;
            if(currentLerpTime >= lerptime)
            {
                currentLerpTime = lerptime;
            }
            percent = currentLerpTime/lerptime;
            blueFade.GetComponent<CanvasGroup>().alpha = Mathf.Lerp(0f, 1f, percent);
            yield return null;
        }

        // Change positions back
        if(isArObject)
        {
            // Is returned arobject, destroy it here and reenable arsending
            Destroy(objectToChange);
            GameObject.Find("ReloadARSessionOnScene").GetComponent<PublicAndGlobalARVariables>().arObjectSent = false;
        }
        else
        {
            objectToChange.transform.localPosition = objectOldPos;
        }


        // Fade back
        currentLerpTime = 0f;   //Reset lerp values
        percent = 0f;
        while(percent < 1)  // Move the object a little bit every frame until destination reached
        {
            currentLerpTime += Time.deltaTime;
            if(currentLerpTime >= lerptime)
            {
                currentLerpTime = lerptime;
            }
            percent = currentLerpTime/lerptime;
            blueFade.GetComponent<CanvasGroup>().alpha = Mathf.Lerp(1f, 0f, percent);
            yield return null;
        }

        blueFade.SetActive(false);
    }
}
