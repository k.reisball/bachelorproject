﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class ARObjectSender : MonoBehaviourPun, IPunObservable
{
    [HideInInspector] public int objectToSend = 0; // 0 is none, 1 is Boulder, 2 is Stairs, etc. <- Gets written to from ARObjectManger
    [HideInInspector] public Vector3 objectPos = Vector3.zero;
    string lastHighlighted = null;
    VR_ReceiveARObject vrobjectreceiverscript = null;

    public void newSceneLoadFunction()
    {
        if(!photonView.IsMine)
        {
            // Get reference to VR Object receiver
            vrobjectreceiverscript = GameObject.Find("VRNetworkReceiver").GetComponent<VR_ReceiveARObject>();
        }
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if(stream.IsWriting)    // Network owner writes values here
        {
            if(objectToSend != 0)
            {
                // Send Data only when the object is not of type 0
                stream.SendNext(objectToSend);
                stream.SendNext(objectPos);
                objectToSend = 0;
            }
        }
        else    // Network Receiver
        {
            objectToSend = (int)stream.ReceiveNext();
            objectPos = (Vector3)stream.ReceiveNext();
            // Make sure the received data is new/not zero, if it is call ar push function
            if(objectToSend != 0)
            {
                // If received data is not null, call Objectplacer
                if(vrobjectreceiverscript == null)
                {
                    newSceneLoadFunction();
                    if (vrobjectreceiverscript == null)
                        return;
                }

                vrobjectreceiverscript.placeObject(objectToSend, objectPos);
            }
        }
    }
}
