﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PublicAndGlobalARVariables : MonoBehaviour
{
    [HideInInspector] public bool arObjectPlaced = false;
    [HideInInspector] public bool arObjectSent = false;
}
