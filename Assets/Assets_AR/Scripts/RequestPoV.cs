﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RequestPoV : MonoBehaviour
{
    ARSendPoV arpovsender = null;

    // Start is called before the first frame update
    void Start()
    {
        // Get reference to AR-PoV-Script
        arpovsender = GameObject.Find("ARPoVSender").GetComponent<ARSendPoV>();
    }

    public void OnPoVButtonpress()
    {
        // Could add coroutine to lock button after pressing for a while here, to prevent spamming

        if(arpovsender == null)
        {
            arpovsender = GameObject.Find("ARPoVSender").GetComponent<ARSendPoV>();
            if (arpovsender == null)
                return;
        }

        arpovsender.ARstartSending = true;
    }
}
