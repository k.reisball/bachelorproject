﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraImageToTexture : MonoBehaviour
{
    // Alternative way to display VR view, gets rid of mesh clipping and light through walls, but is way too dark

    [SerializeField] RawImage targetimage;
    Texture2D screenshot;
    Rect rect = new Rect(0, 0, 720, 405);

    private void Awake() 
    {
        screenshot = new Texture2D(720, 405, TextureFormat.RGB24, false);
        targetimage.texture = screenshot;
    }

    private void OnPostRender() 
    {
        // Read pixels from cameras render texture to Texture 2D
        screenshot.ReadPixels(rect, 0, 0);
        screenshot.Apply();
    }
}
