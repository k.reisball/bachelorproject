﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wingflap : MonoBehaviour
{
    [SerializeField] bool wingside;
    [SerializeField] float lerptime = 1f;
    float start_y = 10f;
    float temp_y;
    float end_y = -10f;
    float start_z = 20f;
    float temp_z;
    float end_z = -30f;
    float currentlerptime = 0f;
    float percent = 0;
    bool down = true;
    Vector3 newpos;

    // Update is called once per frame
    void Update()
    {
        if(down == true)    // down
        {
            if(percent < 1)
            {
                currentlerptime += Time.deltaTime;
                if(currentlerptime >= lerptime)
                {
                    currentlerptime = lerptime;
                }

                percent = currentlerptime/lerptime;
                temp_y = Mathf.Lerp(start_y, end_y, percent);
                temp_z = Mathf.Lerp(start_z, end_z, percent);
                
            }
            else
            {
                percent = 0;
                currentlerptime = 0;
                down = !down;
            }
        }
        else    // up
        {
            if(percent < 1)
            {
                currentlerptime += Time.deltaTime;
                if(currentlerptime >= lerptime)
                {
                    currentlerptime = lerptime;
                }

                percent = currentlerptime/lerptime;
                temp_y = Mathf.Lerp(end_y, start_y, percent);
                temp_z = Mathf.Lerp(end_z, start_z, percent);
                
            }
            else
            {
                percent = 0;
                currentlerptime = 0;
                down = !down;
            }
        }

        newpos = transform.localEulerAngles;

        if(wingside)
        {
            newpos = new Vector3(newpos.y, temp_y, temp_z);
        }
        else
        {
            newpos = new Vector3(newpos.y, -temp_y, -temp_z);
        }

        transform.localEulerAngles = newpos;
        
    }
}
