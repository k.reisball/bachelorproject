This project was created by me, Ken Neubacher, for use in my Bachelor Thesis. Apart from my own contributions the project contains following free Unity Asset Story assets:
- "SteamVR"
- "Quick Outline"
- "FREE Skybox Cubemap Extended Shader • Standard Edition"
- "Photon PUN 2""
- And also imported the "ARCore" Unity Package provided by Google